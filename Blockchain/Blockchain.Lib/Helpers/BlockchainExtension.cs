﻿using Blockchain.Lib.Structure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace Blockchain.Lib.Helpers
{
    public static class BlockchainExtension
    {
        public static byte[] GenerateHash(this IBlock block)
        {
            using (SHA512 sha = new SHA512Managed())
            using (MemoryStream ms = new MemoryStream())
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                bw.Write(block.Data);
                bw.Write(block.Nonce);
                bw.Write(block.TimeStamp.ToBinary());
                bw.Write(block.PreviousHash);
                var memoryStreamArray = ms.ToArray();

                return sha.ComputeHash(memoryStreamArray);
            }
        }

        /// <summary>
        /// Mine the hash based on the given difficulty
        /// </summary>
        /// <param name="block">The block for the mining</param>
        /// <param name="difficulty">The given difficulty</param>
        /// <returns>Mined hash</returns>
        public static byte[] MineHash(this IBlock block, byte[] difficulty)
        {
            if (difficulty == null) throw new ArgumentNullException(nameof(difficulty));

            byte[] hash = new byte[0];

            while (!hash.Take(2).SequenceEqual(difficulty))
            {
                block.Nonce++;
                hash = block.GenerateHash();
            }

            return hash;
        }

        /// <summary>
        /// Validate the block by computing the hash
        /// </summary>
        /// <param name="block">The block to validate</param>
        /// <returns>The boolean value</returns>
        public static bool IsValid(this IBlock block)
        {
            var blockHash = block.GenerateHash();

            return block.Hash.SequenceEqual(blockHash);
        }

        /// <summary>
        /// Validate the previous block by computing the hash and compare it with the current block
        /// </summary>
        /// <param name="block">The current block</param>
        /// <param name="previousBlock">The block to validate</param>
        /// <returns>The boolean value</returns>
        public static bool IsValidPreviousBlock(this IBlock block, IBlock previousBlock)
        {
            if (previousBlock == null) throw new ArgumentNullException(nameof(previousBlock));

            var previousBlockHash = previousBlock.GenerateHash();

            return block.IsValid() && block.PreviousHash.SequenceEqual(previousBlockHash);
        }

        /// <summary>
        /// Validate all blocks in the sequence
        /// </summary>
        /// <param name="items">The items to validate</param>
        /// <returns>The boolean value</returns>
        public static bool IsValid(this IEnumerable<IBlock> items)
        {
            var enumerable = items.ToList();

            return enumerable.Zip(enumerable.Skip(1), Tuple.Create).All(block => block.Item2.IsValid() && block.Item2.IsValidPreviousBlock(block.Item1));
        }
    }
}
