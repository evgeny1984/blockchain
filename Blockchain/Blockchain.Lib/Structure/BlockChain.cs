﻿using Blockchain.Lib.Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Blockchain.Lib.Structure
{
    public class BlockChain : IEnumerable<IBlock>
    {
        private List<IBlock> _Items;

        public BlockChain(byte[] difficulty, IBlock genesisBlock)
        {
            _Items = new List<IBlock>();
            Difficulty = difficulty;
            // Calculate the hash based on the defined difficulty
            genesisBlock.Hash = genesisBlock.MineHash(difficulty);
            Items.Add(genesisBlock);
        }

        #region Properties

        public List<IBlock> Items { get => _Items; set => _Items = value; }

        public int Count => Items.Count;

        public IBlock this[int index]
        {
            get => Items[index];
            set => Items[index] = value;
        }

        public byte[] Difficulty
        {
            get;
        }

        #endregion

        #region Functions

        public void Add(IBlock item)
        {
            item.PreviousHash = Items.LastOrDefault()?.Hash;
            // Calculate the hash based on the defined difficulty
            item.Hash = item.MineHash(Difficulty);
            Items.Add(item);
        }

        public IEnumerator<IBlock> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        #endregion

    }
}
