﻿using System;

namespace Blockchain.Lib.Structure
{
    public class Block : IBlock
    {
        public Block(byte[] data)
        {
            Data = data ?? throw new ArgumentNullException(nameof(data));
            Nonce = 0;
            PreviousHash = new byte[] { 0x00 };
            TimeStamp = DateTime.Now;
            MaxTransations = 1;
        }

        #region Properties

        public byte[] Data { get; }

        public byte[] Hash { get; set; }

        /// <summary>
        /// A number that can only be used once - in cryptography is a one-time code, 
        /// vibrant overcome or pseudorandom manner, which is used to biopsy the main to do transmission, preventing to take the power of reproduction.
        /// </summary>
        public int Nonce { get; set; }

        public byte[] PreviousHash { get; set; }

        public DateTime TimeStamp { get; }

        public int MaxTransations { get; }

        #endregion

        #region Functions

        public override string ToString()
        {
            return $"Hash: {BitConverter.ToString(Hash).Replace("-", "")}\nPrevious hash: {BitConverter.ToString(PreviousHash).Replace("-", "")}\nNonce: {Nonce}\nTimeStamp: {TimeStamp}";
        }

        #endregion

    }
}
