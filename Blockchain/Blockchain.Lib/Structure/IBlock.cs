﻿using System;

namespace Blockchain.Lib.Structure
{
    public interface IBlock
    {
        byte[] Data { get; }
        byte[] Hash { get; set; }
        int Nonce { get; set; }
        byte[] PreviousHash { get; set; }
        DateTime TimeStamp { get; }
        int MaxTransations { get; }
    }
}
