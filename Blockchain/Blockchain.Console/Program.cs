﻿using Blockchain.Lib.Helpers;
using Blockchain.Lib.Structure;
using System;
using System.Linq;

namespace Blockchain.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random(DateTime.UtcNow.Millisecond);
            IBlock genesisBlock = new Block(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00 });
            byte[] difficulty = new byte[] { 0x00, 0x00 };

            BlockChain blockChain = new BlockChain(difficulty, genesisBlock);

            // Generate blocks
            for (int i = 0; i < 200; i++)
            {
                var data = Enumerable.Range(0, 2256).Select(p => (byte)rnd.Next());
                blockChain.Add(new Block(data.ToArray()));
                Console.WriteLine(blockChain.LastOrDefault()?.ToString());
                Console.WriteLine($"Chain is valid: {blockChain.IsValid()}");
                Console.WriteLine("\n");
            }

            Console.ReadLine();
        }
    }
}
